locals {
  purpose               = "OT-Services"
  type                  = "OT-Services"
  owner                 = "Mukesh kumar"
  name                  = "OT-Services"
  all_public_subnetsId  = [module.public_subnet["ap-southeast-2a"].subnetId, module.public_subnet["ap-southeast-2b"].subnetId]
  all_private_subnetsId = [module.private_subnet["ap-southeast-2a"].pvtSubnetId, module.private_subnet["ap-southeast-2b"].pvtSubnetId]
}

module "vpc" {
  source = "./modules/vpc"
  cidr   = var.vpc_cidr
  tags = {
    Name    = "${local.name}-vpc"
    Owner   = local.owner
    Purpose = local.purpose
  }
}

module "public_subnet" {
  source   = "./modules/subnet"
  vpcId    = module.vpc.vpcId
  for_each = var.cidr_public
  AZ         = each.key
  cidr       = each.value
  publicIp   = true
  publicRtId = module.publicRT.igw-id
  tags = {
    Name    = "publicSubnet-${each.key}"
    Owner   = local.owner
    Purpose = local.purpose
  }
}

module "private_subnet" {
  source   = "./modules/subnet-Pvt"
  vpcId    = module.vpc.vpcId
  for_each = var.cidr_private
  AZ       = each.key
  cidr     = each.value
  #publicIp              = false
  privateRtId = module.privateRT.nat-id
  tags = {
    Name    = "privateSubnet-${each.key}"
    Owner   = local.owner
    Purpose = local.purpose
  }
}

module "igw" {
  source = "./modules/igw"
  vpcId  = module.vpc.vpcId
  tags = {
    Name    = "${local.name}-igw"
    Owner   = local.owner
    Purpose = local.purpose
  }
}

module "publicRT" {
  source = "./modules/route_table"
  vpcId  = module.vpc.vpcId
  igw-id = module.igw.igwId
  tags = {
    Name    = "${local.name}-publicRT"
    Owner   = local.owner
    Purpose = local.purpose
  }
}

module "privateRT" {
  source = "./modules/routeTable-pvt"
  vpcId  = module.vpc.vpcId
  nat-id = module.nat-gt.nat-id
  tags = {
    Name    = "${local.name}-privateRT"
    Owner   = local.owner
    Purpose = local.purpose
  }
}

module "nat-gt" {
  source   = "./modules/NAT"
  subnetID = module.public_subnet["ap-southeast-2a"].subnetId
  nat-tags = {
    Name    = "Nat-gatway-${local.name}"
    Owner   = local.owner
    Purpose = local.purpose
  }
}

module "securityGroup" {
  source = "./modules/Security_group"
  vpcId  = module.vpc.vpcId

  tags = {
    Name    = "public-sg"
    Owner   = local.owner
    Purpose = local.purpose
  }
}

module "Ot_Services-SG" {
  source = "./modules/sg_application"
  vpcId  = module.vpc.vpcId
  security_groups = [module.securityGroup.securityGroupID]
  tags = {
    Name    = "${local.name}-Application-SG"
    Owner   = local.owner
    Purpose = local.purpose
  }
}

module "data-sg" {
  source = "./modules/sg_dataBase"
  vpcId  = module.vpc.vpcId
  security_groups = [module.Ot_Services-SG.securityGroupID]
  tags = {
    Name    = "${local.name}-Data-SG"
    Owner   = local.owner
    Purpose = local.purpose
  }
}

module "Public_instance" {
  source        = "./modules/EC2_instance"
  count         = 1
  publicIP      = true
  instance_type = "t2.micro"
  key_name      = "sydney"
  user_data     = file("./ansible.sh")
  ec2-tags = {
    Name    = "FrontEnd-${format("%02d", count.index + 1)}"
    Owner   = local.owner
    Purpose = "bestion_host"
  }
  securitygroupIds = [module.securityGroup.securityGroupID]
  subnetId         = local.all_public_subnetsId[count.index]
}

module "Attendance" {
  source        = "./modules/EC2_instance"
  count         = 1
  publicIP      = false
  instance_type = "t2.micro"
  key_name      = "sydney"
  user_data     = <<-EOF
#!/bin/bash
echo "Installing update"
sudo apt update
EOF
  ec2-tags = {
    Name    = "${local.name}-${format("%02d", count.index + 1)}"
    Owner   = local.owner
    Purpose = local.purpose
    type    = local.type
  }
  securitygroupIds = [module.Ot_Services-SG.securityGroupID]
  subnetId         = local.all_private_subnetsId[count.index]
}

module "Salary" {
  source        = "./modules/EC2_instance"
  count         = 1
  publicIP      = false
  instance_type = "t2.micro"
  key_name      = "sydney"
  user_data     = <<-EOF
#!/bin/bash
echo "Installing update"
sudo apt update
EOF
  ec2-tags = {
    Name    = "Salary-${format("%02d", count.index + 1)}"
    Owner   = local.owner
    Purpose = local.purpose
    type    = local.type
  }
  securitygroupIds = [module.Ot_Services-SG.securityGroupID]
  subnetId         = local.all_private_subnetsId[count.index]
}

module "Employee" {
  source        = "./modules/EC2_instance"
  count         = 1
  publicIP      = false
  instance_type = "t2.micro"
  key_name      = "sydney"
  user_data     = <<-EOF
#!/bin/bash
echo "Installing update"
sudo apt update
EOF
  ec2-tags = {
    Name    = "Employee-${format("%02d", count.index + 1)}"
    Owner   = local.owner
    Purpose = local.purpose
    type    = local.type
  }
  securitygroupIds = [module.Ot_Services-SG.securityGroupID]
  subnetId         = local.all_private_subnetsId[count.index]
}

module "My_SQL" {
  source        = "./modules/EC2_instance"
  count         = 2
  publicIP      = false
  instance_type = "t2.micro"
  key_name      = "sydney"
  user_data     = <<-EOF
#!/bin/bash
echo "Installing update"
sudo apt update
EOF
  ec2-tags = {
    Name    = "My_SQL-${format("%02d", count.index + 1)}"
    Owner   = local.owner
    Purpose = local.purpose
    type    = local.type
  }
  securitygroupIds = [module.data-sg.securityGroupID]
  subnetId         = local.all_private_subnetsId[count.index]
}

module "ElasticSearch" {
  source        = "./modules/EC2_instance"
  count         = 1
  publicIP      = false
  instance_type = "t2.micro"
  key_name      = "sydney"
  user_data     = <<-EOF
#!/bin/bash
echo "Installing update"
sudo apt update
EOF
  ec2-tags = {
    Name    = "ElasticSearch-${format("%02d", count.index + 1)}"
    Owner   = local.owner
    Purpose = local.purpose
    type    = local.type
  }
  securitygroupIds = [module.data-sg.securityGroupID]
  subnetId         = local.all_private_subnetsId[count.index]
}