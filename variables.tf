variable "vpc_cidr" {
  type    = string
  default = "10.10.0.0/18"
}

variable "cidr_public" {
  type = map(string)
  default = {
    ap-southeast-2a = "10.10.32.0/20"
    ap-southeast-2b = "10.10.48.0/21"
  }
}
variable "cidr_private" {
  type = map(string)
  default = {
    ap-southeast-2a = "10.10.56.0/22"
    ap-southeast-2b = "10.10.60.0/22"
    # ap-southeast-2a = "10.10.56.0/23"
    # ap-southeast-2b = "10.10.58.0/23"
    # ap-southeast-2a = "10.10.60.0/23"
    # ap-southeast-2b = "10.10.62.0/23"
  }
}
