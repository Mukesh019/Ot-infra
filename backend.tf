terraform {
  backend "s3" {
    bucket         = "kafka-infra-tool"
    key            = "global/s3/terraform.tfstate"
    region         = "ap-southeast-2"
    dynamodb_table = "kafkaTerraform-state-locking"
  }
}
