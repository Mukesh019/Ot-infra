
resource "aws_eip" "elasticIP" {
} 
resource "aws_nat_gateway" "nat-Gateway" {
  # connectivity_type         = "public"
  allocation_id             = aws_eip.elasticIP.id
  subnet_id                 = var.subnetID
  # subnet_id                 = element(var.subnets_for_nat_gw,1)
  # depends_on                = [aws_internet_gateway.igw]
  tags                      = var.nat-tags 
}

