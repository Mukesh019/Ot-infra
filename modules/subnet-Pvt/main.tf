resource "aws_subnet" "subnetPvt" {
  vpc_id                  = var.vpcId
  cidr_block              = var.cidr
  tags                    = var.tags
  availability_zone       = var.AZ
  #map_public_ip_on_launch = var.publicIp
}
resource "aws_route_table_association" "b" {
  subnet_id               = aws_subnet.subnetPvt.id
  route_table_id          = var.privateRtId
}
