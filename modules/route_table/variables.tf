variable "vpcId" {
  type               = string
  description        = "require VPC ID for routeTable"
  default            = ""
}
variable "tags" {
  default            = {}
  description        = "tags for Route Table"
  type               = map(string)
}
variable "igw-id" {
  type               = string
  default            = ""
  description        = "for gatewayId"
}
# variable "nat-id" {
#   type               = string
#   default            = ""
#   description        = "for gatewayId"
# }