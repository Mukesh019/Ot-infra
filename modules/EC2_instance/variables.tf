
variable "ec2-tags" {
  type              = map(string)
  description       = "tags for EC2 instance"
  default           = {
    # Name            = "kafka",
    # Owner           = "Mukesh",
    # purpose         = "kafka-cluster"
  }
}

variable "key_name" {
  description       = "Key Pair for EC2 instance"
  default           = ""
}

variable "publicIP" {
  type              = bool
  default           = false
}

variable "subnetId" {
  #type = string

}

variable "securitygroupIds" {
  #type = list(string)
 }

 variable "user_data" {
  # type = multiline(string)
   
 }

variable "instance_type" {
  
}





  