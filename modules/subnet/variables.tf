
variable "vpcId" {
  default     = ""
  type        = string
}
variable "cidr" {
  type        = string
  description = "CIDR block for subnet"
  default     = ""
}
variable "tags" {
  # default     = {}
  description = "tags for subnet"
  type        = map(string)
}
variable "AZ" {
  type = string
  default = ""  
}
variable "publicIp" {
  type = bool
  default = false
}
variable "publicRtId" {
  type = string
  default = ""
  description = "route table-id for associating subnet"
}