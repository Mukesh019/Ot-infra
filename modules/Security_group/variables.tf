variable "vpcId" {
  type        = string
  default     = ""
}

variable "tags" {
  type        = map(string)
}


variable "ingressRoles" {
  type = list(number)
  default =[80,22,443]
}

variable "egressRoles" {
  type = list(number)
  default =[0]
}
