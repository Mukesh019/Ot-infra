
resource "aws_security_group" "public-sg" {
  name        = "traffic-FrontEnd"
  vpc_id      = var.vpcId
  tags        = var.tags
  dynamic "ingress" {
    iterator          = port
    for_each          = var.ingressRoles
    content {
    from_port         = port.value
    to_port           = port.value
    protocol          = "TCP"
    cidr_blocks       = ["0.0.0.0/0"]
    }
     
  }

  dynamic "egress" {
    iterator = port
    for_each = var.egressRoles
    content {
    from_port = port.value
    to_port = port.value
    protocol= "-1"
    cidr_blocks = ["0.0.0.0/0"]
    }
  }
}

