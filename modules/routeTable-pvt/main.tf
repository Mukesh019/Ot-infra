
resource "aws_route_table" "private-rt" {
  vpc_id            = var.vpcId
  tags              = var.tags
  route {
    cidr_block      = "0.0.0.0/0"
    gateway_id      = var.nat-id
  }
}

