
variable "tags" {
  type              = map(string)
  description       = "tag for internet gateway "
  default           = {
    Name            = "InternetGateway"
    Owner           = "Mukesh"
  }

}
variable "vpcId" {
  type              = string
  default           = ""
}

