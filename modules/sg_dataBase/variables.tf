
variable "vpcId" {
  type        = string
  default     = ""
}

variable "tags" {
  type        = map(string)
}

variable "ingressRoles" {
  type = list(number)
  default = [9200,3306]
}

variable "egressRoles" {
  type = list(number)
  default =[0]
}

variable "security_groups" {
type = list(string)  
}