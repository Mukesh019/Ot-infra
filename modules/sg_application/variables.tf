
variable "vpcId" {
  type        = string
  default     = ""
}

variable "tags" {
  type        = map(string)
}

variable "ingressRoles" {
  type = list(number)
  default = [8081,8080,8083,3000,22]
}

variable "egressRoles" {
  type = list(number)
  default =[0]
}

variable "security_groups" {
type = list(string)  
}