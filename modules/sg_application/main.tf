
resource "aws_security_group" "OT-service-SG" {
  name        = "OT-service-SG"
  vpc_id      = var.vpcId
  tags        = var.tags
  dynamic "ingress" {
    iterator          = port
    for_each          = var.ingressRoles
    content {
    from_port         = port.value
    to_port           = port.value
    protocol          = "TCP"
    #cidr_blocks       = ["0.0.0.0/0"]
    #security_groups   = ["aws_security_group.public-sg.id"]
    security_groups   = var.security_groups 
    }     
  }

  dynamic "egress" {
    iterator = port
    for_each = var.egressRoles
    content {
    from_port = port.value
    to_port = port.value
    protocol= "-1"
    cidr_blocks = ["0.0.0.0/0"]
    }
  }
}

