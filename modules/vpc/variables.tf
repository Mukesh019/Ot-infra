variable "cidr" {
  type        = string
  description = "CIDR block for VPC"
  default     = ""
}
variable "tags" {
  default     = {}
  description = "tags for vpc"
  type        = map(string)
}
